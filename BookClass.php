<?php
  // Child class
  class Book extends Product {
    public function add_product(): void {
        // Product value is set usiing weghit
        $product_value="Weghit:".$_POST['weight']." KG";

        $sql = "INSERT INTO products (sku, name, price, product_type, product_value)
        VALUES (:sku, :name, :price,:product_type, :product_value)";
        
        $stmt = DB::connect()->prepare($sql);
        $stmt->execute(array(
          ':sku' => $this->get_sku(),
          ':name' => $this->get_name(),
          ':price' => $this->get_price(),
          ':product_type' => $this->get_product_type(),
          ':product_value' => $product_value));
      }
  }
?>
