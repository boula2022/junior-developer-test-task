<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Junior Developer Test Task</title>
        <!-- Bootstrap -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
        <!-- Font Awesome JS -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
        <!-- JQUERY -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>

        <!-- Product list heading, Add button and Mass Delete button -->
        <div class="row" style="padding-top: 3%;">
            <div class="col-md-8"> <h1>Product List</h1></div>
            <div class="col-md-2"> <a href="Add.php"><button>ADD</button></div></a> 
            <!-- This form is created to collect IDs of the products that would be selected in an array and send it to  mass_delete.php by get method  -->
            <form action="mass_delete.php" method="GET">
                <button type="submit" id="delete-product-btn">MASS DELETE</button>
                <hr style="width:100%; height:2px;border-width:0; color:gray;background-color:gray">
                <div class="row"> 
                    <?php  
                        require("ProductClass.php");
                        //Using the show() static method inside the Product class to show all products
                        Product::show();
                    ?>
                </div>
            </form>
        </div>
    </body>
</html>