<?php
  require("BookClass.php");
  require("DVDClass.php");
  require("FurnitureClass.php");
  require("DBClass.php");
  //The parent abstract class 
  abstract class Product{
        
        abstract public function add_product() : void;
        protected $sku,$name,$price,$product_type,$product_value;

        public function set_sku($sku){
          return $this->sku=$sku;
        }
        public function get_sku(){
          return $this->sku;
        }
        public function set_name($name){
          return $this->name=$name;
        }
        public function get_name(){
          return $this->name;
        }
        public function set_price($price){
          return $this->price=$price;
        }
        public function get_price(){
          return $this->price;
        }
        public function set_product_type($product_type){
          return $this->product_type=$product_type;
        }
        public function get_product_type(){
          return $this->product_type;
        }
        public function set_product_value($product_value){
          return $this->product_value=$product_value;
        }
        public function get_product_value(){
          return $this->product_value;
        }
    
        //mass_delete() function recives array with selected products ids to delete them 
        public static function mass_delete(){ 
          $rowCount = count($_GET['products']);
          for($i=0;$i<$rowCount;$i++){
            DB::connect()->query("DELETE FROM products WHERE id=".$_GET['products'][$i]);
          }
        }

        public  function show(){
          $stmt=DB::connect()->query("SELECT * FROM products");
          while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) ) {
            $id=$row['id'];
            //Here I create object from one of the child classes according to the product type recived for each product
            $product=new $row['product_type']();
            $product->set_sku($row['sku']);
            $product->set_name($row['name']);
            $product->set_price($row['price']);
            $product->set_product_value($row['product_value']);

             echo'<div style=" height: 160px; padding-left:25px;  font-size:12pt; "  class="col-md-3">
                    <div style="padding: 5px; margin:10px; border-style:solid;">';
                  echo'<input class="delete-checkbox" type="checkbox" value="'.$id.'"name="products[]"><br>';
                  echo'<span>'.$product->get_sku().'</span><br>';
                  echo'<span>'.$product->get_name().'</span><br>';
                  echo'<span>'.$product->get_price().'</span><br>';
                  echo'<span>'.$product->get_product_value().'</span><br>';
                echo'</div>
                  </div>';
           }
        }   
    }
?>
    