<?php
    //Child class
    class DVD extends Product {
        public function add_product(): void {
            // Product value in this class is set using the input size
            $product_value=$_POST['size']." MB";

            $sql = "INSERT INTO products (sku, name, price, product_type, product_value)
            VALUES (:sku, :name, :price,:product_type, :product_value)";
            
            $stmt = DB::connect()->prepare($sql);
            $stmt->execute(array(
            ':sku' => $this->get_sku(),
            ':name' => $this->get_name(),
            ':price' => $this->get_price(),
            ':product_type' => $this->get_product_type(),
            ':product_value' => $product_value));
        }   
    } 
?>
