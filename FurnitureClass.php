<?php
  class Furniture extends Product {
    public function add_product(): void {
      //product value in this class is set using input Height, Width, and Length
      $product_value="Dimensions:".$_POST['height']."x".$_POST['width']."x".$_POST['length'];
      
      $sql = "INSERT INTO products (sku, name, price, product_type, product_value)
      VALUES (:sku, :name, :price,:product_type, :product_value)";
       
      $stmt = DB::connect()->prepare($sql);
      $stmt->execute(array(
        ':sku' => $this->get_sku(),
        ':name' => $this->get_name(),
        ':price' => $this->get_price(),
        ':product_type' => $this->get_product_type(),
        ':product_value' => $product_value));
        
    }
  }
?>
