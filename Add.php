<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <title>Junior Developer Test Task</title>
        </head>
        <body>

            <div class="row"><div class="col-md-12"><h3>Add Product</h3></div></div>
            <div class="row">
                <form action="add_product.php" id="product_form" method="POST">
                    <div class="form-group">
                        <label for="sku"> SKU </label>
                        <input id="sku" name="sku" type="text">
                    </div><br>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input id="name" name="name" type="text">
                    </div><br>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input id="price" name="price" type="text">
                    </div><br>
                    <div class="form-group">
                        <label for="type">Type switcher</label>
                        <select name="product_type" id="productType" form="product_form">
                            <option  value="Select Type">Select Type</option>
                            <option  value="DVD">DVD</option>
                            <option     value="Book">Book</option>
                            <option  value="Furniture">Furniture</option>
                        </select>
                        <div id="id"></div><br>
                    </div>
                    <button type="submit">Save</button>
                    <a href="index.php"><button type="button">Cancel</button></a>
                </form>
                <script>
                    $(document).ready(
                    function fun(){
                        $("#productType").change( function add(){    
                            $product_type=$("#productType").val();
                            // Empty all the content inside the div with id ="id"
                            $("#id").empty();
                            //Add the content to the div with id="id" to contains the diffrent product types divs
                            $("#id").append('<div  id="DVD"class="form-group"><p>Size(MB)</p> <input id="size" type="number" name="size"> <p>Please, provide size</p></div>');
                            $("#id").append('<div  id="Book"class="form-group"><p>weight(KG))</p> <input id="weight" type="number" name="weight"> <p>Please, provide weight</p></div>');
                            $("#id").append('<div  id="Furniture"class="form-group"><p>Height(CM)</p> <input id="height" type="number" name="height"> <p>Width(CM))</p> <input id="width" type="number" name="width"> <p>Length(CM)</p> <input id="length" type="number" name="length"> <p>Please, provide dimensions</p></div>');
                            //Hide all the divs
                            $("#DVD").hide();$("#Book").hide();$("#Furniture").hide();
                            //Show only the selected product type div
                            $('#'+$product_type).show();
                        })
                    })
                </script>
            </div>
        </body>
    </html>